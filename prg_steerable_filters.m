%% Steerable Filters
% 
% Description:
%   This program computes steerable filters over a given input image (img) in 
%   order to extract edges to a specific orientation (ang). The program makes 
%   use of Haar-like features, instead of Gaussian derivative operators, to 
%   compute efficiently the basis filters of steerable filters. Particularly, 
%   the program computes horizontal (Hx) and vertical (Hy) oriented features 
%   (Haar-like features) using the integral image (II) of the input image (img). 
%
%   If you make use of this code for research articles, we kindly encourage
%   to cite the reference [1], listed below. This code is only for research
%   and educational purposes.
%
% Steerable filter (Hs):
%   Hs = Hx*cos(ang) + Hy*sin(ang)
%
% Reference:
%   [1] Computation of Rotation Local Invariant Features Using the Integral 
%       Image for Real Time Object Detection. M. Villamizar, A. Sanfeliu and 
%       J. Andrade-Cetto. International Conference on Pattern Recognition (ICPR). 
%       Hong Kong, 2006.
%
% Contact:
%   Michael Villamizar
%   mvillami-at-iri.upc.edu
%   Institut de Robòtica i Informática Industrial CSIC-UPC
%   Barcelona - Spain
%   2014
%

%% Main function
function prg_steerable_filters()
clc,close all,clear all

% message
fun_messages('Steerable Filters','presentation');
fun_messages('Steerable Filters','title');

% parameters
hs = 20;  % Haar filter size (integer and even)
fs = 10;  % font size
ang = pi;  % filter orientation
%imgPath = './images/sunset.jpg';  % image file path
imgPath = './images/circle.jpg';  % image file path

% check filter size
if (mod(hs,2)~=0), fun_messages('the filter size must be even and integer','error'); end

% input image
img = imread(imgPath);

% image size
[sy,sx,nc] = size(img);

% message
fun_messages('parameters:','process');
fun_messages(sprintf('filter size -> %d',hs),'information');
fun_messages(sprintf('filter orientation -> %.2f [degrees]',ang*180/pi),'information');
fun_messages('input image:','process');
fun_messages(sprintf('image size -> [%d x %d]',sy,sx),'information');
fun_messages(sprintf('num.channels -> %d',nc),'information');

% show image
figure,imshow(img),title('Input Image','fontsize',fs),xlabel(sprintf('Size -> [%d x %d]',sy,sx),'fontsize',fs);

% convert to gray-scale image
if (nc==3), img = rgb2gray(img); end

% compute the integral image over the input image: img->II
tic; II = mex_img2II(double(img)); t1 = toc;

% compute the Haar-like features in the integral image (II)
tic; [Hx,Hy] = mex_haar_features(II,hs); t2 = toc;

% steerable filter (Hs) using Haar-like features (Hx,Hy) as basis filters
Hs = Hx.*cos(ang) + Hy.*sin(ang);

% messages
fun_messages('times:','process');
fun_messages(sprintf('img -> II : %.5f [sec.]',t1),'information');
fun_messages(sprintf('II -> {Hx,Hy} : %.5f [sec.]',t2),'information');

% show image
figure,subplot(131),imagesc(Hx),colormap(gray),xlabel('Hx','fontsize',fs);
subplot(132),imagesc(Hy),colormap(gray),xlabel('Hy','fontsize',fs);
subplot(133),imagesc(Hs),colormap(gray),xlabel('Hs','fontsize',fs);
figure,imagesc(Hs),colormap(hsv),xlabel('Hs','fontsize',fs);

% message
fun_messages('end','title');
end

%% messages
% This function prints a specific message on the command window
function fun_messages(text,message)
if (nargin~=2), error('incorrect input parameters'); end

% types of messages
switch (message)
    case 'presentation'
        fprintf('****************************************************\n');
        fprintf(' %s\n',text);
        fprintf('****************************************************\n');
        fprintf(' Michael Villamizar\n mvillami@iri.upc.edu\n');
        fprintf(' http://www.iri.upc.edu/people/mvillami/\n');
        fprintf(' Institut de Robòtica i Informàtica Industrial CSIC-UPC\n');
        fprintf(' c. Llorens i Artigas 4-6\n 08028 - Barcelona - Spain\n 2014\n');
        fprintf('****************************************************\n\n');
    case 'title'
        fprintf('****************************************************\n');
        fprintf('%s\n',text);
        fprintf('****************************************************\n');
    case 'process'
        fprintf('-> %s\n',text);
    case 'information'
        fprintf('->     %s\n',text);
    case 'warning'
        fprintf('-> %s !!!\n',text);
    case 'error'
        fprintf(':$ ERROR : %s\n',text);
        error('program error');
end
end

